package mk.iwec.main;

import java.util.ArrayList;
import java.util.List;

import mk.iwec.student.Student;

public class App {

	public static void main(String[] args) {
		List<Student> studentsList = new ArrayList<>();
		Student s1 = new Student(4, "Pero", "Perov");
		studentsList.add(new Student(1, "Darko", "Mirchevski"));
		studentsList.add(new Student(2, "Andrej", "Mirchevski"));
		studentsList.add(new Student(3, "Dean", "Joshevski"));
		studentsList.add(s1);
		System.out.println(studentsList);
		studentsList.remove(0);
		studentsList.remove(s1);
		System.out.println(studentsList);
	}

}
